## 实证金融 (Empirical Finance) 课程主页

- 课件下载：
  - FTP 地址：ftp://ftp.lingnan.sysu.edu.cn/ &rarr; [2019_实证金融] 文件夹。
  - 账号 = 密码 = lianyjst
  - 请保护版权，未经本人同意，请勿散布于网络。
- [【作业】](https://gitee.com/arlionn/EF/wikis/%E6%8F%90%E4%BA%A4%E6%96%B9%E5%BC%8F%E5%92%8C%E6%97%B6%E7%82%B9.md?sort_id=1628096) 

---
### News

- `2019/9/12 15:43`
第一次作业已经发布，参见 「[HW1.md](https://gitee.com/arlionn/EF/wikis/%E6%8F%90%E4%BA%A4%E6%96%B9%E5%BC%8F%E5%92%8C%E6%97%B6%E7%82%B9.md?sort_id=1628096)」 



